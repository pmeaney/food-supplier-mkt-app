

var json_example = { 
  "location_name": "Bobs Grocery Wholesale Market", 
  "location_address": "280 Old County Rd", 
  "location_city": "brisbane", 
  "location_state": "ca",
  "location_country": "USA",
  "location_zip": "94005",

  // location_type can be: supplier, consumer
  "location_type": "supplier", 

  // location_subtype can be: greengrocer, butcher, agrarian, ranch, fisherman, farm, and others to be determined/added
  "location_subtype": "greengrocer", 
  "location_latitude": "37.6852468", 
  "location_longitude": "-122.402772", 
  "isDeleted": false 
},